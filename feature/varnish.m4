# SERVICE(varnish[,keywords])
# ---------------------------
# Monitor number of requests to varnish and cache hits.
#
SERVICE(varnish_hits,notify=no)
SERVICE(varnish_requests,notify=no)

