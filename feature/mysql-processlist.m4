# FEATURE(mysql-processlist, TOT_WARN, TOT_CRIT)
# ----------------------------------------------
# Monitor total and active MySQL threads.  Positional arguments supply
# thresholds for mysql-total-threads.
# 
SERVICE(mysql-total-threads,KW_ARGV(2),KW_ARGV(3))
SERVICE(mysql-active-threads)
