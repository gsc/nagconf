# FEATURE(default[, keywords])
# ----------------------------
# Provides the default set of services: uptime, systime, ping903, ssh, la,
# iface, swap, and cpu.
#
# Named parameters for particular services can be provided using 
# keywords prefixed with the service name, e.g.:
#
# FEATURE(default, ping903_notify=no)
#
# Keywords:
#
#   iface=NAME
#     Network interface name for SERVICE(iface).
#
SERVICE(uptime)
SERVICE(systime)
SERVICE(ping903)
SERVICE(ssh)
SERVICE(la)
SERVICE(iface, KWARG_VALUE(iface, eth0), use=dense)
SERVICE(swap)
SERVICE(cpu, warning=50, critical=80)
