# FEATURE(sendmail)
# -----------------
# Monitor sendmail metrics: queue usage and number of outgoing messages.
#
SERVICE(sendmail-queue, warning=20, critical=100)
SERVICE(sendmail-messages, warning=2, critical=5)
