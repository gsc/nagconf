# FEATURE(http[, ARGS])
# ---------------------
# Default http-listener service with a dependency on the http process.
# See service/http-listener.m4 for the description of ARGS.
#

KWARGS_DUP
KWARG_IFSET([description],[],
[KWARG_SET([description],[http]KWARG_IFSET([tls],[s])[://]KWARG_VALUE(host,HOSTADDR)[]KWARG_VALUE(url,/))])
NC_AT_HOST_END([VHOST_DEP(]KWARG_VALUE(description)[)])
SERVICE(http-listener)
KWARGS_POP()
