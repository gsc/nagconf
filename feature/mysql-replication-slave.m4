# FEATURE(mysql-replication-slave)
# --------------------------------
# Monitor MySQL replication parameters on replication slave.
#
SERVICE(mysql-replication-io)
SERVICE(mysql-replication-sql)
SERVICE(mysql-replication-delay)
