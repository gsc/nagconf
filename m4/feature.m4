# NC_FEATURE_NAME
# ---------------
# This macro holds the name of the feature being defined (first argument to
# FEATURE(), see below.
#
m4_define([NC_FEATURE_NAME])

# _FEATURE(NAME, ARGS...)
# -----------------------
# Expands to the service definition from feature/NAME.m4.  ARGS are accessible
# via the `KW_ARGV' macro.
m4_define([_FEATURE],
[nc__pushdivert(-1)m4_include(feature/$1.m4)nc__popdivert])

# FEATURE(NAME, ARGS...)
# ----------------------
# Expands to the feature definition from feature/NAME.m4
m4_define([FEATURE],[
_GR_CHECK_HOST([FEATURE])
m4_pushdef([NC_FEATURE_NAME], $1)
KWARGS_APPLY([_$0],$@)
m4_popdef([NC_FEATURE_NAME])
])
