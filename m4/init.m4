divert(-1) dnl -*- m4 -*-
include(kwargs.m4)

# Diversion stack.
# The NC__D__ macro keeps the number of the previous diversion.
# Macros nc__pushdivert and nc__popdivert manipulate the stack of diversions.

# nc__pushdivert(N)
# -----------------
# Push current diversion on stack and start diverting output to N.
m4_define([nc__pushdivert], [m4_pushdef([NC__D__], m4_divnum)m4_divert($1)])

# nc__popdivert
# -------------
# Pop the diversion off the top of stack and switch to it.
m4_define([nc__popdivert], [m4_divert(NC__D__)m4_popdef([__D__])])

# nc__once(NAME, TEXT)
# --------------------
# Expand TEXT exactly once.  Once it is expanded, NAME is defined to
# prevent any other expansions.  NAME need not be a valid m4 identifier:
# any characters not valid in an identifier will be replaced with underscore.
m4_define([nc__once],[
m4_pushdef([varname], m4_patsubst([$1], [[^a-zA-Z0-9_]], [_]))
m4_ifdef(m4_defn([varname]),,[
m4_define([m4_defn([varname])],1)
$2
])
m4_popdef([varname])
])

m4_include(host.m4)
m4_include(service.m4)
m4_include(feature.m4)
m4_include(vhost_dep.m4)

# Everything after LOCAL_CONFIG will be copied to the cfg file verbatim.
m4_define(LOCAL_CONFIG,[m4_divert(1)])

m4_define([conf_default_service],[gs])
m4_define([conf_http_servers],[httpd,apache2,lighttpd])
# Optionally include local configuraion settings.
m4_sinclude(config.m4)

m4_divert(0)m4_dnl
# ############################################################################
#
m4_divert(-1)
