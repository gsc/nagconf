# Stack of macros to be expanded at the end of host definition.
m4_pushdef([__at_host_end_tos])

# NC_AT_HOST_END(TEXT)
# -----------------
# Schedule TEXT to be expanded at the end of the current host definition.
m4_pushdef([NC_AT_HOST_END],[
m4_pushdef([__at_host_end_tos],[$1])])

# nc__at_host_end_run()
# -----------------
# Expand everything scheduled with NC_AT_HOST_END.  Entries are expanded in
# the LIFO order.
m4_pushdef([nc__at_host_end_run],[
m4_ifdef([__at_host_end_tos],[nc__pushdivert(-1)__at_host_end_tos[]nc__popdivert
m4_popdef([__at_host_end_tos])m4_dnl
nc__at_host_end_run])])

# HOST(NAME, KWARGS)
# ------------------
# Start a host definition. Normally this is the first non-comment thing in
# a nc file.
#
# NAME is the host name or IP address.
# Keyword arguments are:
#   address
#        Host IP address or name, if different from the NAME.
#        If not supplied, the hostname will be constructed by concatenating
#        NAME, dot, and the value of the `domain' keyword.  If the latter is
#        not defined, NAME alone will be used.
#   domain
#        Domain address to append to the NAME in order to obtain the host
#        address.
#   hostgroups
#        Comma-delimited list of hostgroup names for the host definition.
#   use
#        Comma-delimited list of Nagios template names to use.  Defaults to
#        linux-server,pnp-host (FIXME: should be globally configurable).
#   alias
#        Alias name for that host.
#   contact_groups
#        Comma or whitespace-delimited list of contact group names for that
#        host.  The contact group `admins' is added to the beginning of that
#        list unconditionally (FIXME: should it?)
#   global_*
#        Any keywords starting with `global_' are allowed.  The part after
#        the `global_' prefix must be a valid Nagios keyword.  For each such
#        keyword, the prefix is removed and a line is formed by concatenating
#        variable name, whitespace and the keyword value.  All such lines are
#        concatenated into a single text block with newline between each pair.
#        That block is available for inclusion as the HOST_GLOBAL_OPTIONS macro
#        (see below).
#        This macro is expanded in the SERVICE macro.
#        This way, global parameters common for all service definitions within
#        the host can be defined.
#
m4_define([HOST],[
m4_dnl Bail out on duplicate HOST entries
m4_ifdef([HOSTNAME],[m4_errprint(m4___file__:m4___line__: duplicate [HOST] (missing [ENDHOST]?)
)
m4_exit(1)])
m4_dnl Save the host name and parse the keyword arguments.
m4_pushdef([HOSTNAME],$1)
KWARGS_PUSH(m4_shift($@))
m4_pushdef([HOSTADDR],KWARG_VALUE([address],[HOSTNAME[]KWARG_IFSET([domain],[.]KWARG_VALUE([domain]))]))
m4_dnl Initialize the `atexit' stack.
m4_pushdef([NC_AT_HOST_END_LIST])
m4_dnl Emit the host definition:
nc__pushdivert(0)
define host {
KWARG_IFSET([use],[        use	     KWARG_VALUE([use])],[        use	     linux-server,pnp-host])

        host_name    HOSTNAME
KWARG_IFSET([alias],[	    alias     KWARG_VALUE([alias])
])m4_dnl
        address	     HOSTADDR
KWARG_IFSET([hostgroups],[        hostgroups   KWARG_VALUE([hostgroups])
])m4_dnl
KWARG_IFSET([contact_groups],[	[contact_groups admins,]m4_patsubst([KWARG_VALUE([contact_groups])],[\s+],[,])])
}
nc__popdivert
m4_dnl Save global keywords.
nc__push_host_global_options
KWARGS_POP])

m4_pushdef([HOST_GLOBAL_OPTIONS])

# nc__push_host_global_options()
# ------------------------------
# Scan keyword definitions, and extract keywords starting with `global_'.  For
# each such keyword form a Nagios statement line, by removing the prefix and
# concatenating the resulting string, whitespace and keyword option. Append
# the result to the value of the macro HOST_GLOBAL_OPTIONS (delimited by a
# newline from the existing content).
m4_define([nc__push_host_global_options],[
m4_pushdef([HOST_GLOBAL_OPTIONS],m4_dnl
KWARGS_FOREACH([opt],[m4_ifelse(m4_patsubst(m4_quote(opt),[^global_.*],[GLOBAL]),[GLOBAL],[        m4_patsubst(opt,[^global_],[])        KWARG_VALUE(opt)
])]))])

# nc__pop_host_global_options()
# -----------------------------
# Forget the definition of HOST_GLOBAL_OPTIONS
m4_define([nc__pop_host_global_options],[m4_popdef([HOST_GLOBAL_OPTIONS])])

# _GR_CHECK_HOST(NAME)
# --------------------
# Auxiliary macro to check that the HOST line appeared above.  If not,
# complains that `HOST must be declared prior to NAME' and exits.
m4_define([_GR_CHECK_HOST],[
m4_ifdef([HOSTNAME],,[m4_errprint(m4___file__:m4___line__: [HOST] must be declared prior to [$1]
)
m4_exit(1)
])])

# nc__endhost()
# -------------
# Macro to be expanded at the end of a host definition.  Reverts all changes
# that HOST did.
m4_define([nc__endhost],[
m4_ifdef([HOSTNAME],[
nc__at_host_end_run
m4_undivert(1)
nc__pop_host_global_options
m4_popdef([HOSTNAME])
m4_popdef([HOSTADDR])])])

# Schedule nc__endhost to be expanded at EOF.
m4_m4wrap([nc__endhost])

# ENDHOST()
# ---------
# End the host definition.  It should be used only if the source file defines
# more than one host.  Otherwise, it can be omitted (and it is the recommended
# practice), since nc__endhost will be expanded at the end of file.
m4_define([ENDHOST],[
m4_ifdef([HOSTNAME],[nc__endhost],[
m4_errprint(m4___file__:m4___line__: [ENDHOST] without [HOST]
)
m4_exit(1)])])

