# VHOST_DEP(SERVICE)
# ------------------
# Auxiliary macro to create dependency on another service.
# Usage:
#   m4_pushdef([nc__http_proc],V)
#   NC_AT_HOST_END([VHOST_DEP(SERVICE)])
#
m4_define([VHOST_DEP],[
m4_ifdef([nc__http_proc],[m4_divert(0)
define servicedependency {
	host_name			HOSTNAME
	service_description		nc__http_proc
	dependent_host_name		HOSTNAME
	dependent_service_description	$1
	execution_failure_criteria	u,c
	notification_failure_criteria	w,u,c
}
])])
