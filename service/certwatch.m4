# SERVICE(certwatch, CN, [CN...], [keywords])
# -------------------------------------------
# Check for the validity of the certificate for CN.
#
# Keywords:
#
#   description
#     Service description.  If absent
#
#   warning
#     Defines warning threshold.  Defaults to 48h.
#
#   critical
#     Defines critical threshold.  Defaults to 24h.
#
m4_pushdef([__cn],[m4_patsubst(m4_quote(m4_shift(KW_ARGS)),[,],[ ])])
m4_pushdef([__servdescr],[KWARG_VALUE([description],[__cn])])

BEGIN_SERVICE
	service_description	Certificate for m4_ifelse(m4_eval(m4_len(m4_quote(__servdescr)) > 24),1,[m4_substr(m4_quote(__servdescr),0,24)...],[__servdescr])
	check_command		check_cert!m4_dnl	
m4_dnl Script arguments:
NC_THRESHOLDS(48h, 24h) m4_dnl
m4_dnl 3. CNs
__cn
END_SERVICE

m4_popdef([__servdescr])
m4_popdef([__cn])

