# SERVICE(tallyman, NAME, keywords)
# ---------------------------------
# Checks tallyman response for service NAME.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description	KWARG_VALUE([description],[KW_ARGV(2)])
	check_command		check_tallyman!NC_DEFCHECK(,1:)
END_SERVICE
