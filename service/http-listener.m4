# SERVICE(http-listener, keywords)
# --------------------------------
# Check if host responds to HTTP requests.
#
# Keywords:
#
#   description
#     Service description.
#
#   host
#     Query this host.  Defaults to HOSTADDR.
#
#   url
#     Query this url.  Defaults to /.
#
#   tls
#     Use TLS.
#
#   expect
#     Expected reply.  Defaults to 200.
#
#   header
#     Expected header.
#
#   content
#     String to expect in the content.
#
#   check_args
#     Additional arguments for check_http

m4_pushdef([__url],KWARG_VALUE(url,/))
m4_pushdef([__hostname],KWARG_VALUE(host,HOSTADDR))
m4_pushdef([__descr],KWARG_VALUE(description, [http]KWARG_IFSET([tls],[s])[://]__hostname[]__url))
BEGIN_SERVICE
	service_description	__descr
	check_command		check_http!m4_dnl
-t KWARG_VALUE(timeout, 10) m4_dnl
-H __hostname m4_dnl
KWARG_IFSET([url],[-u "__url" ])m4_dnl
KWARG_IFSET([expect],[-e KWARG_VALUE(expect) ])m4_dnl
KWARG_IFSET([header],[-d KWARG_VALUE(header) ])m4_dnl
KWARG_IFSET([content],[-s KWARG_VALUE(content) ])m4_dnl
KWARG_IFSET([tls],[--ssl]) KWARG_VALUE(check_args)
END_SERVICE

m4_popdef([__hostname])
m4_popdef([__url])
m4_popdef([__descr])
