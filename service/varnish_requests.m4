# SERVICE(varnish_requests)
# -------------------------
# Check the number of HTTP requests per second as reported by Varnish
# (varnish-mib, to be exact).
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[HTTP req/sec])
	check_command		varnish_requests
END_SERVICE


