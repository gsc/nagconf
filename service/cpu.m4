# SERVICE(cpu[, keywords...])
# ---------------------------
# Check CPU usage.
#
# Keywords:
#
#   warning
#     Defines warning threshold.
#
#   critical
#     Defines critical threshold.
#   
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[CPU Load])
        check_command           check_snmp_cpu!NC_THRESHOLDS
END_SERVICE

