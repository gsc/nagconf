# SERVICE(ows, ENDPOINT, keywords)
# --------------------------------
# Checks accessibility and response times of an OWS endpoint.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[OWS KW_ARGV(2) response time])
        check_command           check_ows_response_time!NC_DEFCHECK
END_SERVICE

