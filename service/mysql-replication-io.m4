# SERVICE(mysql-replication-io)
# -----------------------------
# Checks the status of MySQL replication I/O.
#
BEGIN_SERVICE
	service_description     KWARG_VALUE([description],[MySQL replication IO])
	check_command		check_mysql_replication_io
END_SERVICE
