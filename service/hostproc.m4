# SERVICE(hostproc, PROG, keywords)
# -----------------------------
# Check the number of PROG processes running.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
# 
m4_foreach([p], m4_quote(conf_http_servers), [m4_ifelse(p, KW_ARGV(2),
[m4_define([nc__http_proc],p)
NC_AT_HOST_END([m4_undefine([nc__http_proc])])])])

BEGIN_SERVICE(pnp-svc)
	service_description	KWARG_VALUE([description],[KW_ARGV(2)])
	check_command		check_snmp_hostproc!m4_dnl
KWARG_IFSET([exclude],[ --exclude])[]m4_dnl
KWARG_IFSET([param],[ --param=KWARG_VALUE([param])])[]m4_dnl
 NC_DEFCHECK
END_SERVICE
