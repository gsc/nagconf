# SERVICE(mysql-replication-sql)
# ------------------------------
# Checks whether replication process is running on slave.
#
BEGIN_SERVICE
	service_description     KWARG_VALUE([description],[MySQL replication SQL])
	check_command		check_mysql_replication_sql
END_SERVICE

