# SERVICE(varnish_hits, keywords)
# -------------------------------
# Check Varnish Cache hits ratio.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[varnish hits ratio])
        check_command           check_varnish_ratio!NC_THRESHOLDS(60, 50)
END_SERVICE
