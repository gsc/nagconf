# SERVICE(systime, keywords)
# --------------------------
# Check the system time on the remote machine and compute the difference
# between the time on the local host and the obtained time.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[System Time])
	check_command		check_systime!NC_THRESHOLDS(30,90)
END_SERVICE


