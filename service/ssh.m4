# SERVICE(ssh)
# ------------
# Check ssh accessability
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[SSH])
        service_description     SSH
	check_command		check_ssh
END_SERVICE

