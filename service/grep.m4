# SERVICE(grep, REGEXP, keywords)
# -------------------------------
# Check if the HTTP response from the host matches REGEXP.
#
# Keywords:
#
#   description
#     Service description.
#
#   host
#     Query this host.  Defaults to HOSTADDR.
#
#   url
#     Query this url.  Defaults to /.
#
#   tls
#     Use TLS.
#
#   check_args
#     Additional arguments for check_http
#
m4_pushdef([__url],KWARG_VALUE(url,/))
m4_pushdef([__hostname],KWARG_VALUE(host,HOSTADDR))
m4_pushdef([__descr],KWARG_VALUE(description, [GREP ]KW_ARGV(2) [http]KWARG_IFSET([tls],[s])[://]__hostname[]__url))

# The `notes' keyword is handled by the END_SERVICE macro.  Provide one, unless
# already supplied.
KWARG_IFSET([notes],[],[KWARG_ASGN(notes=__descr)])

BEGIN_SERVICE
	service_description	__descr
	check_command           check_http!-H __hostname -t KWARG_VALUE(timeout,10) -u "__url" -r "KW_ARGV(2)" KWARG_IFSET([tls],[-S]) KWARG_VALUE(check_args)
END_SERVICE

m4_popdef([__hostname])
m4_popdef([__url])
m4_popdef([__descr])
