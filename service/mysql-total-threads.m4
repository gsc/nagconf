# SERVICE(mysql-total-threads, WARN, CRIT)
# ----------------------------------------
# Checks number of threads in the MySQL server.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[MySQL total threads])
	check_command		check_mysql_total_threads!KW_ARGV(2)!KW_ARGV(3)
END_SERVICE
