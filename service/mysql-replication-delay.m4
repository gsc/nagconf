# SERVICE(mysql-replication-delay)
# --------------------------------
# Checks mysql replication delay.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[MySQL replication delay])
	check_command		check_mysql_replication_delay!m4_dnl
NC_THRESHOLDS([:],[:180000])
END_SERVICE
