# SERVICE(swap, keywords)
# -----------------------
# Check swap usage.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
nc__once([_service_swap_]HOSTNAME,[
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Swap Usage])
	check_command		check_snmp_swap!NC_THRESHOLDS(80,90)
END_SERVICE	
])

