# SERVICE(phpsess)
# ----------------
# Monitor the number of simultaneously accessed php sessions
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[PHP Session Usage])
        check_command           check_phpsess
END_SERVICE

