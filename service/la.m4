# SERVICE(la, keywords)
# ---------------------
# Check load average on the host.
#
# Keywords:
#
#   critical
#     Defines critical threshold.  Defaults to 9/7/5.
#
#   warning
#     Defines warning threshold.  Defaults to 12/10/7
#
# Threshold notation is: LA1/LA5/LA15.  Comma can be used instead of slash.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Load Average])
	check_command		check_snmp_la!NC_THRESHOLDS(9/7/5, 12/10/7)
END_SERVICE
