# SERVICE(disk, NAME [, keywords...])
# -----------------------------------
# Expands to a service definition for monitoring the disk usage of
# the mountpoint NAME.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[KW_ARGV(2) usage])
	check_command		check_snmp_du!NC_DEFCHECK
END_SERVICE
