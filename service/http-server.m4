# SERVICE(http-server, NAME, keywords)
# ------------------------------------
# Check if the httpd process NAME is running on host.
#
# Keywords:
#
#   critical
#     Defines critical threshold.  Defaults to "min:max", where
#     min and max are obtained from th oids UCD-SNMP-MIB::prMin and
#     UCD-SNMP-MIB::prMax, correspondingly.
#
#   warning
#     Defines warning threshold.  
#
# Side effects:
#   Defines nc__http_proc to NAME.
#
m4_define([nc__http_proc],KW_ARGV(2))
NC_AT_HOST_END([m4_undefine([nc__http_proc])])

BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[KW_ARGV(2) processes])
	check_command		check_snmp_proc!NC_DEFCHECK
END_SERVICE


