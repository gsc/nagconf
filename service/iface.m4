# SERVICE(iface, NAME, keywords)
# ------------------------------
# Check traffic on network interface NAME.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Traffic on KW_ARGV(2)])
	check_command		check_if_traffic!NC_DEFCHECK
END_SERVICE

