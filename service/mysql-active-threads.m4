# SERVICE(mysql-active-threads)
# -----------------------------
# Check the number of the active MySQL threads.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[MySQL active threads])
	check_command		check_mysql_active_threads!NC_THRESHOLDS
END_SERVICE
