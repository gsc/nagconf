# SESSION(sendmail-messages[, keywords])
# -------------------------------------
# Monitor number of the mail messages in Sendmail.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Sendmail Messages])
        check_command           sendmail_messages!NC_THRESHOLDS
END_SERVICE
