# SERVICE(sendmail-queue[, keywords])
# -----------------------------------
# Monitor Sendmail queue usage.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Sendmail Queue])
        check_command           sendmail_queue!NC_THRESHOLDS
END_SERVICE
