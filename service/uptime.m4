# SERVICE(uptime, keywords)
# -------------------------
# Check remote system uptime.
#
BEGIN_SERVICE
	service_description     KWARG_VALUE([description],[Uptime])
	check_command		check_snmp!--extra-opts -o sysUpTime.0
END_SERVICE
