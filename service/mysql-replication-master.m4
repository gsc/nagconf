# SERVICE(mysql-replication-master, NSLAVE)
# -----------------------------------------
# Checks the number of replication slaves on the master server.
#
BEGIN_SERVICE
	service_description     KWARG_VALUE([description],[MySQL replication slaves])
	check_command		check_mysql_slave_count!KW_ARGV(2)
END_SERVICE

