# SESSION(sendmail-outmesg[, keywords])
# -------------------------------------
# Monitor number of the outgoing mail messages in Sendmail.
#
# Keywords:
#
#   critical
#     Defines critical threshold.
#
#   warning
#     Defines warning threshold.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Sendmail Outgoing Messages])
        check_command           sendmail_messages_outgoing!NC_THRESHOLDS
END_SERVICE

