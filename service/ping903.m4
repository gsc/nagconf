# SERVICE(ping903, keywords)
# --------------------------
# Check ICMP echo using ping903
#
# Keywords:
#
#   critical
#     Defines critical threshold.  Default 600.0,60%.
#
#   warning
#     Defines warning threshold.  Default 200.0,20%
#
#   check_interval
#     Ping check interval.  Default 5.
#
#   retry_interval
#     Retry interval.  Default 1.
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Ping903])
	check_command		check_ping903!NC_THRESHOLDS([200.0,20%],[600.0,60%])
	check_interval	KWARG_VALUE([check_interval], 5)
	retry_interval	KWARG_VALUE([retry_interval], 1)
END_SERVICE

