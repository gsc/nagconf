# SERVICE(memory, keywords)
# -------------------------
# Check memory usage
#
BEGIN_SERVICE(pnp-svc)
	service_description     KWARG_VALUE([description],[Memory usage])
        check_command           check_nrpe!check_mem
END_SERVICE
