help:
	@echo "Available goals are:"
	@echo ""
	@echo "make config PREFIX=DIR"
	@echo "   Configure the project to be installed in DIR/nc."
	@echo "   Notice: The PREFIX argument is mandatory."
	@echo ""
	@echo "make install"
	@echo "   Install files."
	@echo ""
	@echo "Auxiliary goals:"
	@echo ""
	@echo "make bootstrap"
	@echo "   Bootstrap the project."
	@echo ""

bootstrap: incfiles

incfiles: include/composite.m4 include/kwargs.m4 include/prefix.m4

ifneq (,$(wildcard .config))
include .config
endif

config: incfiles
	@if [ -z '$(PREFIX)' ]; then \
		echo "please supply the PREFIX variable with config"; \
		exit 1; \
	else \
		echo "PREFIX=$(PREFIX)" > .config; \
	fi

install: incfiles
	@if [ -z '$(PREFIX)' ]; then \
		echo "please run make config first"; \
		exit 1; \
	else \
		mkdir -p $(PREFIX)/nc; \
		rsync -a Make.rules m4 include feature service nagios $(PREFIX)/nc; \
	fi

include/composite.m4 include/kwargs.m4 include/prefix.m4: m4kwargs/Makefile
	$(MAKE) -C m4kwargs install M4DIR=$$(pwd)/include BRACKETQUOTES=1
include/composite.m4: m4kwargs/m4/composite.m4
include/kwargs.m4: m4kwargs/m4/kwargs.m4
include/prefix.m4: m4kwargs/m4/prefix.m4

m4kwargs/Makefile:
	git submodule init
	git submodule update

